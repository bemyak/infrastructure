output "nextcloud_public_ip" {
  value = oci_core_instance.nextcloud.public_ip
}

output "rollbot_public_ip" {
  value = oci_core_instance.rollbot.public_ip
}

output "nextcloud_private_ip" {
  value = oci_core_instance.nextcloud.private_ip
}

output "rollbot_private_ip" {
  value = oci_core_instance.rollbot.private_ip
}
