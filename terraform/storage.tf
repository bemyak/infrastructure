resource "oci_core_volume" "rollbot" {
    compartment_id = var.tenancy_ocid

    availability_domain = local.availability_domain[0]
    size_in_gbs = 50
}

resource "oci_core_volume_attachment" "rollbot" {
    attachment_type = "iscsi"
    instance_id = oci_core_instance.rollbot.id
    volume_id = oci_core_volume.rollbot.id
}
