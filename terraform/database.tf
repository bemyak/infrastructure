resource "oci_database_autonomous_database" "test_autonomous_database" {
    compartment_id = var.tenancy_ocid
    db_name = "DB"
    admin_password = var.db_password
    db_workload = "OLTP"
    # is_access_control_enabled = true
    is_free_tier = true
    license_model = "LICENSE_INCLUDED"

    whitelisted_ips = [
        resource.oci_core_vcn.rollbotVCN.id
    ]
}
