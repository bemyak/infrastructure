terraform {
  required_version = ">= 0.12.0"
  backend "http" {
    # Address should be passed with the CLI param `-backend-config="address=$TF_STATE_URL"`
    # address       = "https://objectstorage.eu-zurich-1.oraclecloud.com/p/<SUPER_SECURE_HASH>/b/tfstate/o/terraform.tfstate"
    update_method = "PUT"
  }
}

resource "oci_core_instance" "rollbot" {
  availability_domain = local.availability_domain[0]
  compartment_id      = var.tenancy_ocid
  display_name        = "rollbot"
  shape               = local.instance_shape

  create_vnic_details {
    subnet_id        = oci_core_subnet.rollbot.id
    display_name     = "primaryvnic"
    assign_public_ip = true
    hostname_label   = "rollbot"
  }

  source_details {
    source_type = "image"
    source_id   = data.oci_core_images.ubuntu.images[0].id
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }

}

resource "oci_core_instance" "nextcloud" {
  availability_domain = local.availability_domain[0]
  compartment_id      = var.tenancy_ocid
  display_name        = "nextcloud"
  shape               = local.instance_shape

  create_vnic_details {
    subnet_id        = oci_core_subnet.rollbot.id
    display_name     = "primaryvnic"
    assign_public_ip = false
    hostname_label   = "nextcloud"
  }

  source_details {
    source_type = "image"
    source_id   = data.oci_core_images.ubuntu.images[0].id
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }

}

data "oci_core_images" "ubuntu" {
  compartment_id   = var.tenancy_ocid
  operating_system = "Canonical Ubuntu"
  shape            = local.instance_shape
  sort_by          = "TIMECREATED"
  sort_order       = "DESC"
}

// https://docs.cloud.oracle.com/en-us/iaas/images/image/7581ea04-f340-418c-a495-2968437bea8e/
// Canonical-Ubuntu-20.04-Minimal-2020.05.19-0
locals {
  instance_shape      = "VM.Standard.E2.1.Micro"
  availability_domain = [for limit in data.oci_limits_limit_values.test_limit_values : limit.limit_values[0].availability_domain if limit.limit_values[0].value > 0]
}
