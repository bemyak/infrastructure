variable "tenancy_ocid" {}

variable "user_ocid" {}

variable "region" {}

variable "oci_private_key" {}

variable "ssh_public_key" {}

variable "fingerprint" {}

variable "db_password" {}
